#!/usr/bin/python

import webapp
import shelve
import string
import random

url_dict = shelve.open('url_dict')


def generate_random_string(length: str = 6):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


def generate_short_url(url):
    # Add "https://" if it's not already there
    if not (url.startswith("http://") or url.startswith("https://")):
        url = "https://" + url

    # Use the URL as a key in a dictionary
    if url not in url_dict:
        # Generate a random string as the short URL
        short_url = generate_random_string(6)
        while short_url in url_dict.values():
            short_url = generate_random_string(6)
        # Add the original URL and the short URL to the dictionary
        url_dict[url] = short_url
    else:
        # If the URL is already in the dictionary return the existing short URL
        short_url = url_dict[url]

    return short_url


def get_long_url(short_url):
    for url, value in url_dict.items():
        if value == short_url:
            return url
    return None


class shortenerApp(webapp.webApp):

    def parse(self, request):
        lines = request.splitlines()
        firstLineWords = lines[0].split(' ', 2)
        verb = firstLineWords[0]
        resource = firstLineWords[1]
        separator = lines.index('')
        try:
            bodyFirstLine = lines[separator + 1]
        except IndexError:
            bodyFirstLine = None
        return verb, resource, bodyFirstLine

    def process(self, reqData):
        (verb, resource, reqBody) = reqData
        # print(reqData)
        if verb == "GET":
            if resource == "/":
                items = []
                for key in url_dict:
                    items.append((key, url_dict[key]))

                htmlBody = """
                    <form action="/" method="post">
                    url to shorten: <input type="text" name="url">
                    <input type="submit" value="Submit">
                    </form>
                    <body>
                        {items}
                    </body>""".format(items=items)
                httpCode = "200 OK"

                return httpCode, "<html><body>" + htmlBody + "</body></html>"
            else:
                short_url = resource[1:]
                long_url = get_long_url(short_url)
                if long_url:
                    httpCode = "302 Found\r\n" + "Location: {long_url}\r\n\r\n".format(long_url=long_url)
                    htmlBody = ""

                    return httpCode, htmlBody
                else:
                    htmlBody = """
                        <!DOCTYPE html>
                        <html lang="en">
                          <body>
                            <p>ERROR</p>
                          </body>
                        </html>
                        """
                    httpCode = "404 Not Found"

                    return httpCode, "<html><body>" + htmlBody + "</body></html>"

        elif verb == "POST":
            params = reqBody.split('&')
            url = None
            for param in params:
                (name, value) = param.split("=", 1)
                if name == "url":
                    url = value
                    short_url = generate_short_url(url)
            items = []
            for key in url_dict:
                items.append((key, url_dict[key]))
            htmlBody = """
                <form action="/" method="post">
                url to shorten: <input type="text" name="url">
                <input type="submit" value="Submit">
                </form>
                <body>
                    {items}
                </body>""".format(items=items)
            httpCode = "200 OK"
            return httpCode, "<html><body>" + htmlBody + "</body></html>"


if __name__ == "__main__":
    testWebApp = shortenerApp("localhost", 1234)
